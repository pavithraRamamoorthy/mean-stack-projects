const mongoose = require('mongoose');

const profileSchema = new mongoose.Schema({
    firstName: {
        type: String,
        maxLength: 100,
        required: true
    },
    lastName: {
        type: String,
        maxLength: 100,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    mobileNo: {
        type: String,
        required: true,
        unique: true
    },
    profilePicture: {
        type: String
    },
    documents: [String],
})

const Profile = mongoose.model('profile', profileSchema);

// Custom validation for email
profileSchema.path('email').validate((val) => {
    emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRegex.test(val);
}, 'Invalid e-mail.');  

module.exports = Profile