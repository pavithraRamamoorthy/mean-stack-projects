import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Routes, RouterModule } from '@angular/router';
import { UploadFileComponent } from './upload-file/upload-file.component';
import { FilesListComponent } from './files-list/files-list.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'add-file' },
  { path: 'add-file', component: UploadFileComponent },
  { path: 'files-list', component: FilesListComponent }
]

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
