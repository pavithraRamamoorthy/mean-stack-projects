export class FileModel {
    id: string;
    name: string;
    uploadedFile: string;
}