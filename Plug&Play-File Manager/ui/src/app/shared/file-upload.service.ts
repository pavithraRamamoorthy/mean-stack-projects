import { Injectable } from '@angular/core';
import { FileModel } from './file';
import { environment } from '../../environments/environment';
import { HttpHeaders, HttpErrorResponse, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class FileUploadService {

  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }

  // Get Files
  getFiles() {
    return this.http.get(environment.apiBaseUrl)
  }

  // UploadFile
  addFile(name: string, files: File) {
    var formData = new FormData();
    formData.append("name", name);
    formData.append("uploadedFile", files);

    return this.http.post<FileModel>(`${environment.apiBaseUrl}/upload`, formData, {
      reportProgress: true,
      observe: 'events'
    })
  }

}