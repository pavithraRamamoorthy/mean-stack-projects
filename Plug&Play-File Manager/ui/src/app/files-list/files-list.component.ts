import { Component, OnInit } from '@angular/core';
import { FileUploadService } from "../shared/file-upload.service";

@Component({
  selector: 'app-files-list',
  templateUrl: './files-list.component.html',
  styleUrls: ['./files-list.component.css']
})
export class FilesListComponent implements OnInit {

  Files: any = [];

  constructor(public fileUploadService: FileUploadService) {
    this.getFiles();
  }

  ngOnInit() { }

  getFiles() {
    this.fileUploadService.getFiles().subscribe((res) => {
      this.Files = res['Files'];
    })
  }
}