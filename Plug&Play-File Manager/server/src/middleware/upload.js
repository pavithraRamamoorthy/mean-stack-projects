const util = require("util");
const multer = require("multer");
// //const GridFsStorage = require("multer-gridfs-storage");
// Multer File upload settings
const DIR = './public/';

var storage =   multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, DIR);
  },
  filename: (req, file, cb) => {
    const fileName = file.originalname.toLowerCase().split(' ').join('-');
    cb(null, fileName)
  }
});

var uploadFile = multer({ storage: storage}).single("uploadedFile");
var uploadFilesMiddleware = util.promisify(uploadFile);
module.exports = uploadFilesMiddleware;


