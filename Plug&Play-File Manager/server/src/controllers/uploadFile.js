const upload = require("../middleware/upload");
var mongoose = require('mongoose');
// File model
let File = require('../models/file');

const uploadFile = async (req, res) => {
  try {
    await upload(req, res);
    const url = req.protocol + '://' + req.get('host')
    const file = new File({
      _id: new mongoose.Types.ObjectId(),
      name: req.body.name,
      uploadedFile: url + '/public/' + req.file.filename
    });
    file.save().then(result => {
      console.log(result);
      res.status(201).json({
        message: "successfully uploaded a File",
        fileCreated: {
          _id: result._id,
          name: result.name,
          uploadedFile: result.uploadedFile
        }
      })
    });
  }catch(err) {
      console.log(err),
        res.status(500).json({
          error: err
        });
    }
};  
module.exports = {
  uploadFile: uploadFile
};