const upload = require("../middleware/upload");

// File model
let File = require('../models/file');

var getFile = (req, res, next) => {
        File.find().then(data => {
                res.status(200).json({
                        message: "Files retrieved successfully!",
                        Files: data
                });
        });       
};

module.exports = {
        getFile: getFile
};