var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');
var db = require('./src/database/db');
var logger    = require('./src/utils/logger');
var config    = require('config');


// Routes to Handle Request
const fileRoute = require('./src/routes/routes')


// Setup Express.js
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cors());

app.use(function(request, response, next){
  logger.info("Request Body ", request.body)
  logger.info("Request Query ", request.query)
  logger.info("Request Headers ", request.headers)
  let responseData = response.send;
  response.send   = function(data){
      logger.info(JSON.parse(data))
      responseData.apply(response, arguments)
  }    
  next();
})


// Make files "Uploads" Folder Publicly Available
app.use('/public', express.static('public'));


// API Route
app.use('/api', fileRoute)


let port = config.get('port')

if(port){
  app.listen(port, function(){
    logger.info("Server is listening at port : " + port);
  })
}
else{
  logger.error("No servert port is set. Check the config/Env setup.")
}