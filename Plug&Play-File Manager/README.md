## How to run the app?
- Start the Angular Application
  -`cd ui` to enter into the ui folder
    - Run `npm install` to install required dependencies.
    - Run `ng serve` to run the angular app

- Start Node server
  - `cd server` to enter into the server folder
    - Run `npm install` to install required dependencies.
    - `nodemon server` to start the nodemon server
    - `mongod` to start the mongoDB shell
